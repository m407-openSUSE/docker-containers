# openSUSE image supporting systemd

This is a Docker image capable of running systemd.

This work is based on [this Fedora](https://github.com/fedora-cloud/Fedora-Dockerfiles/tree/master/systemd/systemd) Dockerfile.

## Building

To run docker in a container you need to mount cgroup file system volume:

```
# docker run --detach --stop-signal SIGRTMIN+3 --tmpfs /run -v /sys/fs/cgroup:/sys/fs/cgroup:ro opensuse/systemd
```

To test once inside the container, check and see if systemd is working:

```
# /usr/lib/systemd/systemd --system
```